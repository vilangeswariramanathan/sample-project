package com.alation.api.models;

import java.io.Serializable;

public class ImporterStatus implements Serializable {
	
	private String fileName;
	private String uploadedTime;
	private String status;
	private String linkToProcess;
	private String filePath;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getUploadedTime() {
		return uploadedTime;
	}
	public void setUploadedTime(String uploadedTime) {
		this.uploadedTime = uploadedTime;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getLinkToProcess() {
		return linkToProcess;
	}
	public void setLinkToProcess(String linkToProcess) {
		this.linkToProcess = linkToProcess;
	}	
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "filename:: "+fileName+" uploadedTime: "+uploadedTime+" status: "+status+" linkToProcess: "+linkToProcess;
	}
	public String getFilePath() {
		return filePath;
	}
	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}
	

}
