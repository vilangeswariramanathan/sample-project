package com.alation.repo.github;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.UUID;



import org.apache.log4j.Logger;

import org.json.simple.parser.JSONParser;


public class StorageManager {
	
	public static Logger loggerr = Logger.getLogger(StorageManager.class);
	
	private String path;

	private String fileName;

	/**
	 * Constructor takes parameters to self manage. Needs S3 Connection, bucket and
	 * new file to manage.
	 * 
	 * @param s3
	 * @param bucketName
	 * @param keyNameToStore
	 * @param logger
	 */
	
	
	public StorageManager(String path, String fileName) {
		this.path = path;
		this.fileName = fileName;
		
	}
		

	public InputStream getContent() {
		try {
			File file = new File(path+fileName);
			if(file.exists()) {
				InputStream targetStream = new FileInputStream(file);
				return targetStream;
			}
			else {
				loggerr.info("Sorry file not exists in location "+path+fileName);
				file.createNewFile();
				loggerr.info("Empty file created");
			}
		} catch (Exception e) {
			loggerr.error(e.getMessage(),e);
		}

		// Return null
		return null;
	}

	public  long appendContentLocally(String content) {
		// TODO Auto-generated method stub
		
		long lineCounter = 0;
		try {
		File temp = File.createTempFile("temp-file-name", ".properties");
		if(!temp.exists()) {
			temp.createNewFile();
		}
		BufferedWriter buffWriter = new BufferedWriter(new FileWriter(temp, true));
		buffWriter.append(content);
		// Close streams
		buffWriter.close();
		
			
		BufferedReader buffReader = new BufferedReader(new InputStreamReader(new FileInputStream(temp)));
		String jsonRecord;
		while ((jsonRecord = buffReader.readLine()) != null) {
			  JSONParser parser = new JSONParser(); 
			  org.json.simple.JSONObject json = (org.json.simple.JSONObject)  parser.parse(jsonRecord);
			  String isDirectory = json.get("is_directory").toString();
			  
			  if (isDirectory.equals("false")) {
				  lineCounter++;
			  }
			
		}
		buffReader.close();
		
		}
		catch(Exception e) {
			loggerr.error(e.getMessage(),e);
		}		
	return lineCounter;	
		
	}

		
	
}
