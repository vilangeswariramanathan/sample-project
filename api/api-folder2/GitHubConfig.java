package com.alation.repo.github;

import static com.alation.git.github.util.GitHubConstants.*;
//import static com.alation.git.github.util.GitHubConstants.REPOSITORY_PARTIAL_URL;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.alation.api.utils.ApiUtils;
import com.alation.api.utils.PropHelper;
import com.alation.git.github.util.GitHubConstants;
import com.alation.git.github.util.GithubUtils;
import com.alation.github.model.AlationConfig;

public class GitHubConfig {
	private static final Logger logger = Logger.getLogger(GitHubConfig.class);

	private static ApiUtils apiutils = null;
	public static final String NEW_LINE_CHAR = "\n";

	public static JSONArray get_all_repos(String accesstoken, String Reposiory_url) throws Exception {

		JSONArray repositoryJSONArray = apiutils.doGet(Reposiory_url, accesstoken);
		if (repositoryJSONArray == null || repositoryJSONArray.isEmpty()) {

			logger.info("No Repository found");
		}
		return repositoryJSONArray;
	}
	

	public static JSONArray getRepocontent(String accessToken, String content_url) throws Exception {
		// TODO Auto-generated method stub

		JSONArray repocontentJSONArray = apiutils.doGet(content_url, accessToken);

		if (repocontentJSONArray == null || repocontentJSONArray.isEmpty()) {

			logger.info("No Content exist in Repository ");
		}
		return repocontentJSONArray;

	}
	
		

	public static JSONArray getRepoFileContentTree(LinkedList<String> reponmList) throws Exception {
		String repositoryOwner = getrepositoryOwner();
		String accessToken = getAccessToken();
		String Reponame = null;
		
		JSONArray repocontentJSONArray = new JSONArray();
		JSONArray content = new JSONArray();
		LinkedHashMap<String,String> RepositoryKeyMap=new LinkedHashMap<String,String>();
		LinkedHashMap<String,String> RepositoryandFileMap=new LinkedHashMap<String,String>();
		for (int i = 0; i < reponmList.size(); i++) {
			Reponame = reponmList.get(i);
			String Git_tree_url = REPOSITORY_PARTIAL_URL + "/" + repositoryOwner + "/" + Reponame + "/" + GITHUB_GIT_URL
					+ "/" + GITHUB_TREE_URL + "/" + "master?recursive=1";
			repocontentJSONArray = GithubUtils.doGetRepoContent(Git_tree_url, accessToken);
			
			
			//code to get the Repository & Key Map
			
			RepositoryKeyMap=getRepositorySummaries(repocontentJSONArray,Reponame);
			RepositoryandFileMap.putAll(RepositoryKeyMap);
			//code to get the Repository & Key Map
			

			content.addAll(repocontentJSONArray);

		}

		return content;

	}

	

	public static JSONArray getRepoFileContent(String reponame_type, String name, String sha, String foldername)
			throws Exception {
		// TODO Auto-generated method stub
		// https://api.github.com/repos/Alation-alias/HelloWorld/git/blobs/b5056783233b56511635e0d2ccedaff343e82303
		
		JSONArray FileContent = new JSONArray();
		String repositoryOwner = getrepositoryOwner();
		
		String File_Content_Url = REPOSITORY_PARTIAL_URL + "/" + repositoryOwner + "/" + foldername + "/"
				+ GITHUB_GIT_URL + "/" + GITHUB_FILE_TYPE_BLOB + "/" + sha;
		HttpResponse 	response = ApiUtils.doGET(PropHelper.getHelper().getGitHubbaseURL(),  getAccessToken(), File_Content_Url);
		if (response.getStatusLine().getStatusCode() != 200) {
			logger.fatal("Invalid HTTP Response, returning null.");
			logger.error(response);
			
		} else {
			FileContent.add((JSONObject) new JSONParser().parse(ApiUtils.convert(response.getEntity().getContent())));

		}
		return FileContent;
	}

	public static String decodeString(JSONArray fileContent) {
		JSONObject gitHubData=new JSONObject();
		for (int i = 0; i < fileContent.size(); i++) 
         gitHubData = (JSONObject) fileContent.get(i);
     return  new String((Base64.getMimeDecoder()).decode(gitHubData.get(GitHubConstants.CONTENT).toString()));

	}

	public static String getAccessToken() throws Exception {
	
		return "Bearer " + PropHelper.getHelper().getGitHubToken();
	}
	

	public static String getrepositoryOwner() throws Exception {
		// TODO Auto-generated method stub
		return  PropHelper.getHelper().getGitHubOwner();
	}

	public static Map<StringBuffer, String> getPathNameMap(String reponame_content) {
		// TODO Auto-generated method stub

		String[] path = null;

		StringBuffer Path = new StringBuffer();

		Map<StringBuffer, String> PathNameMap = new HashMap<StringBuffer, String>();

		path = reponame_content.split("/");
		logger.info("String[] size:::::::" + path.length);

		for (int i = 0; i < path.length - 1; i++) {
			Path = Path.append(path[i]);
			Path.append("/");
		}
		String name = path[path.length - 1];

		PathNameMap.put(Path, name);
		return PathNameMap;
	}


	private static LinkedHashMap<String,String> getRepositorySummaries(JSONArray repocontentJSONArray, String reponame) {
		// TODO Auto-generated method stub
		String repositoryType="";
		String FilePath="";
		String Key="";
		//LinkedList<String> RepositoryAndFile=new LinkedList<String>();
		LinkedHashMap<String,String> RepositoryAndFile=new LinkedHashMap<String,String>();
		for (int index = 0; index < repocontentJSONArray.size(); index++) 
		{
			JSONObject RepositoryArray = (JSONObject) repocontentJSONArray.get(index);
			repositoryType = RepositoryArray.get(GitHubConstants.TYPE).toString();
			if(repositoryType.equalsIgnoreCase("blob"))
			{
				FilePath=RepositoryArray.get(GitHubConstants.PATH).toString();
				Key=GitHubConstants.PATH_CHAR+FilePath;
				RepositoryAndFile.put(Key,reponame);
				
			}
		}
		return RepositoryAndFile;
	}

	
	//code
	public static LinkedHashMap<String,String> getRepoFileContentTreeMap(LinkedList<String> reponmList) throws Exception {
		String repositoryOwner = getrepositoryOwner();
		String accessToken = getAccessToken();
		String Reponame = null;
		
		JSONArray repocontentJSONArray = new JSONArray();
		JSONArray content = new JSONArray();
		LinkedHashMap<String,String> RepositoryKeyMap=new LinkedHashMap<String,String>();
		LinkedHashMap<String,String> RepositoryandFileMap=new LinkedHashMap<String,String>();
		for (int i = 0; i < reponmList.size(); i++) {
			Reponame = reponmList.get(i);
			String Git_tree_url = REPOSITORY_PARTIAL_URL + "/" + repositoryOwner + "/" + Reponame + "/" + GITHUB_GIT_URL
					+ "/" + GITHUB_TREE_URL + "/" + "master?recursive=1";
			repocontentJSONArray = GithubUtils.doGetRepoContent(Git_tree_url, accessToken);
			
			logger.info("repocontentJSONArray::::::::==" + repocontentJSONArray);
			//code to get the Repository & Key Map
			
			RepositoryKeyMap=getRepositorySummaries(repocontentJSONArray,Reponame);
			RepositoryandFileMap.putAll(RepositoryKeyMap);
			//code to get the Repository & Key Map
			

			content.addAll(repocontentJSONArray);

		}

		return RepositoryandFileMap;

	}
	//code


	public static String getDeltaPayload(List<String> alationJsonArr) {
		StringBuffer buff = new StringBuffer();
		// Append new data
		for (String str : alationJsonArr) {
			buff.append(str + NEW_LINE_CHAR);
		}
		return buff.toString();
		// TODO Auto-generated method stub
		
	}


	public static long appendContentLocally(String content, String path) {
		// TODO Auto-generated method stub
	// TODO Auto-generated method stub
		
		long lineCounter = 0;
		try {
		//File file = new File(bucketName+key);
			File temp = new File(path);
		
		BufferedWriter buffWriter = new BufferedWriter(new FileWriter(temp, true));
		buffWriter.append(content);
		// Close streams
		buffWriter.close();
		
		/*if(key.equalsIgnoreCase(AlationConfig.ALATION_DATA_STORE_NAME))
		{*/	
		BufferedReader buffReader = new BufferedReader(new InputStreamReader(new FileInputStream(temp)));
		String jsonRecord;
		while ((jsonRecord = buffReader.readLine()) != null) {
			  JSONParser parser = new JSONParser(); 
			  org.json.simple.JSONObject json = (org.json.simple.JSONObject)  parser.parse(jsonRecord);
			  String isDirectory = json.get("is_directory").toString();
			  
			  if (isDirectory.equals("false")) {
				  lineCounter++;
			  }
			
		}
		buffReader.close();
		//}
		}
		catch(Exception e) {
			logger.error(e.getMessage(),e);
		}		
	return lineCounter;	
		
	}

}
