package com.alation.repo.github;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import com.alation.api.utils.ConfigHelper;
import com.alation.api.utils.PropHelper;
import com.alation.git.github.util.GitHubConstants;
import com.alation.github.http.AlationProcessor;
import com.alation.github.http.JsonUtils;
import com.alation.github.metadata.MetadataHandle;
import com.alation.github.metadata.MetadataHandler;
import com.alation.github.model.AlationBulkMetadataUpdate;
import com.alation.github.model.AlationConfig;
import com.alation.github.model.AlationFS;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Files;


public class GitHubGenericRepositoryProvider {

	// Static Stuff
	private static final Logger logger = Logger.getLogger(GitHubGenericRepositoryProvider.class);

	// Configuration Helper
	private ConfigHelper configHelper = new ConfigHelper();

	private MetadataHandler handler = new MetadataHandler();

	// Alation Processor
	private AlationProcessor alationProcessor = null;

	private StorageManager vfsjobStoreManager = null;

	

	public static void main(String[] args) throws Exception {

		logger.info(new Date() + " STARTING!");

		// Create new Instance
		GitHubGenericRepositoryProvider repositoryProvider = new GitHubGenericRepositoryProvider();

		// Kick start!
		repositoryProvider.kickstartThis();
	}

	public void kickstartThis() throws Exception {

		// Setup configurations
		// This class builds AlationConfig object and abstracts from accessing directly!
		getConfigHelper().createConfig();

		// Verify basic Credentials
		getConfigHelper().verifyDefaults();

		// Alation Processor
		alationProcessor = new AlationProcessor(getAlationConfig());

		// start processing Repositories
		processGitHubConnector();

	}
	
	
	public GitLabClient(String host, String token, String project) throws GitException {
		String  host1 = host;
		String  token1 = token;
		  this.connect = GitlabAPI.connect(host, token);
		  // init gitlab project if project name given
		  if (project != null) {
		    setCurrentProject(project);
		  }
		}
	
	

	public void processGitHubConnector() throws Exception {

		// START
		logger.info(new Date() + " STARTING!");
		LinkedHashMap<String, String> RepositoryFileMap = new LinkedHashMap<String, String>();

		JSONArray repositories = getRepository();

		LinkedHashMap<String, String> TempPath = new LinkedHashMap<String, String>();

		JSONArray SubFolderContentByType = new JSONArray();

		// Get the Git Tree and create the relationship between Directories and files
		JSONArray subFoldersTree = GitHubConfig.getRepoFileContentTree(getReponameList(repositories));

		RepositoryFileMap = GitHubConfig.getRepoFileContentTreeMap(getReponameList(repositories));

		// Return the TempFile Path which will be created in the Local
		TempPath = getTempFilePath(subFoldersTree);

		SubFolderContentByType = getSubFolderFiles(subFoldersTree);

		processRepositoryFolder(RepositoryFileMap, repositories, TempPath, SubFolderContentByType);
		
		logger.info(" ENDS! Open for Business");

	}

	private LinkedHashMap<String, String> getTempFilePath(JSONArray subFoldersTree) throws Exception {
		// TODO Auto-generated method stub
		LinkedHashMap<String, String> TempFilePath = new LinkedHashMap<String, String>();
		for (int j = 0; j < subFoldersTree.size(); j++) {
			JSONObject RepoContentTree = (JSONObject) subFoldersTree.get(j);
			String sha = RepoContentTree.get("sha").toString();
			String reponame_content_Path = RepoContentTree.get(GitHubConstants.PATH).toString();
			String reponame_type = RepoContentTree.get(GitHubConstants.TYPE).toString();
			String url = RepoContentTree.get("url").toString();
			String FolderName = findRepositoryName(url);
			String Content = "";

			if (reponame_type.equalsIgnoreCase("blob")) {
				JSONArray FileContent = GitHubConfig.getRepoFileContent(reponame_type, reponame_content_Path, sha,
						FolderName);
				Content = GitHubConfig.decodeString(FileContent);
				String TempPath = writeContentToTempFile(reponame_content_Path, Content, FolderName);
				TempFilePath.put(reponame_content_Path, TempPath);
			}

		}

		return TempFilePath;
	}

	private String findRepositoryName(String url) {
		// TODO Auto-generated method stub
		String foldername = new String();
		Pattern regex = Pattern.compile("repos/(.*?)/git");
		Matcher regexMatcher = regex.matcher(url);
		while (regexMatcher.find()) {
			for (int i = 1; i <= regexMatcher.groupCount(); i++) {
				String subdata = regexMatcher.group(1);

				foldername = subdata.split("/")[1];

			}
		}
		return foldername;
	}

	public JSONArray getRepository() throws Exception {
		JSONArray repository = null;

		String Reposiory_url = GitHubConstants.GITHUB_USER_URL + "/"
				+ getConfigHelper().getAlationConfig().getGitHubOwner() + "/" + GitHubConstants.REPOSITORY_PARTIAL_URL;
		repository = GitHubConfig.get_all_repos(getConfigHelper().getAlationConfig().getGithubtoken(), Reposiory_url);

		return repository;
	}

	public static LinkedList<String> getReponameList(JSONArray repos) {
		LinkedList<String> reponm = new LinkedList<String>();
		for (int i = 0; i < repos.size(); i++) {
			JSONObject repo = (JSONObject) repos.get(i);
			String reponame = repo.get(GitHubConstants.NAME).toString();
			reponm.add(reponame);
		}
		
		return reponm;
	}

	public AlationConfig getAlationConfig() {
		return getConfigHelper().getAlationConfig();
	}

	private String writeContentToTempFile(String name, String content, String foldername) throws Exception {
		String[] Filepath = null;
		String path =null;
		if (name.contains("/")) {
			Filepath = name.split("/");
			name = Filepath[Filepath.length - 1];

		}

		File myTempFile = new File(Files.createTempDir(), name);
		
		if(name.contains("doc") )
		{
			GenerateDocFile(myTempFile,content);
		}
		else
		{

		String cname = myTempFile.getName();
		String tDir = System.getProperty("java.io.tmpdir");
		BufferedWriter bw = new BufferedWriter(new FileWriter(myTempFile));
		bw.write(content);
		bw.close();
		path = myTempFile.getAbsolutePath();
		}

		System.out.println("path in  writemtd:" + path);
		return path;
	}

	

	private List<AlationBulkMetadataUpdate> processBulkMetdataUpdate(String foldername, String reponame_content,
			MetadataHandle handle, List<AlationBulkMetadataUpdate> alationBulkMetaDataHandle) throws Exception {

		AlationBulkMetadataUpdate metadataUpdate = new AlationBulkMetadataUpdate();
		metadataUpdate.setDescription(handle.getBody());

		// Cook up alation FS Object URI
		StringBuffer key4BulkUpdate = new StringBuffer(String.valueOf(PropHelper.getHelper().getFilesystemid()));
		key4BulkUpdate.append(GitHubConstants.PATH_CHAR);
		key4BulkUpdate.append(foldername);
		key4BulkUpdate.append(GitHubConstants.PATH_CHAR);
		key4BulkUpdate.append(reponame_content);
		metadataUpdate.setKey(key4BulkUpdate.toString());
		alationBulkMetaDataHandle.add(metadataUpdate);
		if (getAlationConfig().getFileSystemId() != null) {

		} else {
			logger.warn("Skipping file system post as filesystem_id is not provided in lambda");
		}

		return alationBulkMetaDataHandle;
		// TODO Auto-generated method stub

	}

	private JSONArray getSubFolderFiles(JSONArray subFoldersTree) {
		// TODO Auto-generated method stub
		JSONArray SubFolderArrayByType = new JSONArray();
		for (int j = 0; j < subFoldersTree.size(); j++) {
			JSONObject repositoryContent = (JSONObject) subFoldersTree.get(j);
			String repositoryType = repositoryContent.get(GitHubConstants.TYPE).toString();
			if (repositoryType.equalsIgnoreCase("blob")) {
				SubFolderArrayByType.add(repositoryContent);
			}


		}

		return SubFolderArrayByType;

	}

	private void processRepositoryFolder(LinkedHashMap<String, String> repositoryFileMap, JSONArray repositories,
			LinkedHashMap<String, String> tempPath, JSONArray subFolderContentByType) {
		// TODO Auto-generated method stub

		int FileCount = repositoryFileMap.size();
		long counter;
		LinkedList<String> alationFSJson = new LinkedList<String>();
		String Path = "";

		for (Map.Entry<String, String> entry : repositoryFileMap.entrySet()) {

			logger.info("Key and Repository Value");
		
			try {
				logger.info("File in progress " + entry.getKey());
				// lastmodified need to set here
				String RepositoryAndFile = "/" + entry.getKey();
				List<AlationFS> alationFSList = JsonUtils.buildAlationFSString(entry, repositories);
				List<String> alationFSJsonString = JsonUtils.createJsonString(alationFSList);
				alationFSJson.addAll(alationFSJsonString);
				alationProcessor = new AlationProcessor(getAlationConfig(), alationFSJson);
				Path = createContentFile();
				InputStream is = TempFileContent(Path);
				alationProcessor.setPreviousPayload(is);
				counter = GitHubConfig.appendContentLocally(GitHubConfig.getDeltaPayload(alationFSJson), Path);
				logger.info("counter:::" + counter);

				if (isEligibleForPost(counter, FileCount)) {
					alationProcessor.doVFSPost(vfsjobStoreManager);
					processBulkMetadata(tempPath, subFolderContentByType);
				}

			} catch (Exception e) {
				logger.error(e.getMessage(), e);
			}
		}

	}

	private String createContentFile() throws IOException {
		// TODO Auto-generated method stub

		File file = File.createTempFile("alation_data", ".properties");
		String Path = file.getAbsolutePath();
		file.deleteOnExit();
		return Path;
	}

	private boolean isEligibleForPost(long currentLine, int FileCount) {
		boolean isEligibleForPost = false;
		try {

			boolean process = ((currentLine == FileCount));

			isEligibleForPost = process;

		} catch (Exception e) {
			logger.error(e.getMessage(), e);

		}
		return isEligibleForPost;
	}

	private void processBulkMetadata(LinkedHashMap<String, String> TempFilePath, JSONArray subFolderContentByType)
			throws Exception {
		String repositoryContentUrl = "";
		String subFoldersPath = "";
		String RepositoryName = "";
		String Path = "";
		String jsonString = "";
		List<AlationBulkMetadataUpdate> alationBulkMetaDataHandle = new ArrayList<AlationBulkMetadataUpdate>();

		for (int j = 0; j < subFolderContentByType.size(); j++) {
			JSONObject repositoryContent = (JSONObject) subFolderContentByType.get(j);
			repositoryContentUrl = repositoryContent.get(GitHubConstants.URL).toString();
			RepositoryName = findRepositoryName(repositoryContentUrl);
			subFoldersPath = repositoryContent.get(GitHubConstants.PATH).toString();
			for (Map.Entry<String, String> entry : TempFilePath.entrySet()) {
				Path = entry.getKey();
			

				if (Path.equalsIgnoreCase(subFoldersPath)) {

					MetadataHandle handle = handler.getHandle(getAlationConfig(), entry.getValue());
					if (handle != null) {
						alationBulkMetaDataHandle = processBulkMetdataUpdate(RepositoryName, subFoldersPath, handle,
								alationBulkMetaDataHandle);
					}

					for (AlationBulkMetadataUpdate alationBulkMetaDataUpdate : alationBulkMetaDataHandle) {
						String jsonappendString = new ObjectMapper().writeValueAsString(alationBulkMetaDataUpdate);
						jsonappendString = jsonappendString.replace("\n", "").replace("\r", "");
						jsonString = jsonString.concat(jsonappendString + System.lineSeparator());
					}
					alationProcessor.doBulkMetadataPost(jsonString);
				}

			}

		}

	}

	public AlationProcessor getAlationProcessor() {
		return alationProcessor;
	}

	public ConfigHelper getConfigHelper() {
		return configHelper;
	}

	public void setConfigHelper(ConfigHelper configHelper) {
		this.configHelper = configHelper;
	}

	private InputStream TempFileContent(String path) throws FileNotFoundException {
		// TODO Auto-generated method stub
		File f = new File(path);

		InputStream inputStream = new FileInputStream(f);
		return inputStream;
	}
	
	private void GenerateDocFile(File myTempFile, String content) {
		try {
			
			FileOutputStream fos = new FileOutputStream(myTempFile.getAbsolutePath());
			XWPFDocument doc = new XWPFDocument();
			XWPFParagraph tempParagraph = doc.createParagraph();
			XWPFRun tempRun = tempParagraph.createRun();

			//tempRun.setText("This is a Paragraph");
			tempRun.setText(content);
			tempRun.setFontSize(12);
			doc.write(fos);
			fos.close();
			
			System.out.println(myTempFile.getAbsolutePath()+ " created successfully!");

		} catch (Exception e) {
			logger.error(e.getMessage(),e);

		}
		// TODO Auto-generated method stub
		
	}

}
